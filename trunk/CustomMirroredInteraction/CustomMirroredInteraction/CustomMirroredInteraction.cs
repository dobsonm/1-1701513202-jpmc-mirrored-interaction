﻿using Genesyslab.Desktop.Infrastructure;
using Genesyslab.Desktop.Infrastructure.Commands;
using Genesyslab.Desktop.Infrastructure.Configuration;
using Genesyslab.Desktop.Modules.CustomMirroredInteraction.CustomCommands;
using Genesyslab.Platform.Commons.Logging;
using System;
using System.Reflection;

namespace Genesyslab.Desktop.Modules.CustomMirroredInteraction
{

    /// <summary>
    /// JPMorgan Chase Agents currently use WDE version 8.5.129.04 to handle customer interactions. A new WDE 
    /// customization is required to prevent “Mirrored” interactions from disturbing Agents. A Mirrored interaction 
    /// is a WDE Workitem that is automatically generated when an Agent performs specific activity in JPMorgan Chase’s 
    /// Integrated Database Management System (IDMS). These Workitems are force routed to WDE for tracking purposes only. 
    /// Agent’s do not interact with the WDE Workitem in any way. The WDE screen pop that occurs for Mirrored Workitems 
    /// interrupts and confuses Agents who are trying to complete their work in IDMS. Furthermore, when the Agents 
    /// complete their work activity in IDMS, the corresponding WDE Workitem is automatically force closed which triggers 
    /// a system error toast to appear causing further frustration.
    /// </summary>
    public class CustomMirroredInteraction : IModule
    {
        public const String TASK_CAN_USE_MIRRORED_TEST_MODE = "InteractionWorkspace.Custom.canUseMirroredTestMode";
        public const String TASK_CAN_USE_MIRRORED_TEST_MODE_DFLT = "0";
        public const String TASK_CAN_USE_MIRRORED_TEST_MODE_TRUE = "1";

        private ICommandManager commandManager_ = null;
        private IConfigManager configManager_ = null;
        private ILogger log_ = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomMirroredInteraction"/> class.
        /// </summary>
        public CustomMirroredInteraction(ICommandManager commandManager, IConfigManager configManager, ILogger log)
        {
            this.commandManager_ = commandManager;
            this.configManager_ = configManager;

            // Create a child trace section.
            this.log_ = log.CreateChildLogger("CustomMirroredInteraction");
            Assembly workspaceAssembly = Assembly.GetEntryAssembly();
            AssemblyName workspaceAssemblyName = workspaceAssembly.GetName();
            String workspaceVersion = workspaceAssemblyName.Version.ToString();

            log_.Info("CustomMirroredInteraction() version=" + workspaceVersion + ", Build=1.1.0.0, Build Date=05/20/2019");
        }

        /// <summary>
        /// Initializes the module.
        /// </summary>
        public void Initialize()
        {
            log_.Debug("Initialize(): Initializing CustomMirroredInteraction Module");

            // Insert custom commands
            insertCustomCommands();
        }

        /// <summary>
        /// Insert custom commands into existing WDE chains of command.
        /// </summary>
        protected void insertCustomCommands()
        {
            try
            {
                /*
                 * Agents with the Role Privilege "Custom - Can Use Mirrored Test Mode" will NOT have their 
                 * Interactions Window hidden.
                 */
                String canUseMirroredTestMode = configManager_.GetValueAsString(TASK_CAN_USE_MIRRORED_TEST_MODE, TASK_CAN_USE_MIRRORED_TEST_MODE_DFLT);
                if (!canUseMirroredTestMode.Equals(TASK_CAN_USE_MIRRORED_TEST_MODE_TRUE))
                {
                    /*
                     * BeforeShowInteractionsWindowCommand will automatically hide the Interactions Window
                     * of a Mirrored Interaction.
                     * Note: ShowInteractionsWindow is not triggered unless interaction-bar.enable-quick-access = false
                     */
                    log_.Debug("insertCustomCommands(): Inserting Custom Command: BeforeShowInteractionsWindowCommand");
                    commandManager_.CommandsByName["ShowInteractionsWindow"].Insert(0,
                        new CommandActivator()
                        {
                            CommandType = typeof(BeforeShowInteractionsWindowCommand),
                            Name = "BeforeShowInteractionsWindowCommand"
                        });
                }
                /*
                 * BeforeShowToasterWindowCommand will block the unwanted system error toast from appearing.
                 * Note: The error will still appear under "My Messages".
                 */
                log_.Debug("insertCustomCommands(): Inserting Custom Command: BeforeShowToasterWindowCommand");
                commandManager_.CommandsByName["ShowMessageToasterWindow"].Insert(0,
                    new CommandActivator()
                    {
                        CommandType = typeof(BeforeShowToasterWindowCommand),
                        Name = "BeforeShowToasterWindowCommand"
                    });
            }
            catch (Exception e)
            {
                log_.Error("insertCustomCommands(): Exception=" + e.Message);
            }
        }
    }
}
