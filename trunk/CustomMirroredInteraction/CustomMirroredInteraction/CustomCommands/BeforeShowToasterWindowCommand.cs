﻿using Genesyslab.Desktop.Infrastructure;
using Genesyslab.Desktop.Infrastructure.Commands;
using Genesyslab.Desktop.Infrastructure.DependencyInjection;
using Genesyslab.Desktop.Modules.Core.Model.Broadcast;
using Genesyslab.Desktop.Modules.Core.Model.Interactions;
using Genesyslab.Desktop.Modules.OpenMedia.Model.Interactions.WorkItem;
using Genesyslab.Desktop.Modules.Windows.Views.Toaster.Broadcast;
using Genesyslab.Platform.Commons.Logging;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;

namespace Genesyslab.Desktop.Modules.CustomMirroredInteraction.CustomCommands
{
    /// <summary>
    /// The following custom WDE implementation will block the unwanted system error toast from appearing. 
    /// Note: The error will still appear under “My Messages”. 
    ///   1.	Insert a new custom command: BeforeShowToasterWindowCommand
    ///       a.	The command is only active if Role Privilege “Custom – Can Use Mirrored Interaction” is allowed.
    ///       b.	Get the IInteraction from the Interaction parameter
    ///           i.	If the IInteraction is IInteractionWorkItem
    ///               1.	Get the IToasterMessageWindow from the CommandParameter
    ///                   a.	Get the BroadcastMessageSystem from the IToasterMessageWindow
    ///                       i.	If the subject of the BroadcastMessageSystem contains “This interaction is now 
    ///                             considered inactive due to handling timeout.” then do not display the popup toast.
    /// </summary>
    class BeforeShowToasterWindowCommand : IElementOfCommand
    {
        private const String COMMAND_PARAMETER = "CommandParameter";
        private const String INTERACTION = "Interaction";
        private const String UNWANTED_SUBJECT = "Call has disconnected. - Media Error (237) - voice, instant messaging (7001@SIP_Switch)";
        private ILogger log_ = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="BeforeShowToasterWindowCommand"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public BeforeShowToasterWindowCommand(IObjectContainer container)
        {
            try
            {
                log_ = container.Resolve<ILogger>();
                // Create a child trace section.
                log_ = log_.CreateChildLogger("Genesyslab.Desktop.Modules.CustomMirroredInteraction.CustomCommands.BeforeShowToasterWindowCommand");
                log_.Debug("Constructor():  START");
            }
            catch (Exception e)
            {
                log_.Error("Constructor():  Exception: " + e.Message);
            }
            log_.Debug("Constructor():  END");
        }

        /// <summary>
        /// Gets the name of the command. This is optional.
        /// </summary>
        /// <value>The command name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>True if the execution is stopped; false if successful.</returns>
        public bool Execute(IDictionary<string, object> parameters, IProgressUpdater progress)
        {
            try
            {
                log_.Debug("Execute():  START");

                // To the main thread.
                if (Application.Current.Dispatcher != null && !Application.Current.Dispatcher.CheckAccess())
                {
                    object result = Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new ExecuteDelegate(Execute), parameters, progress);
                    return (bool)result;
                }
                else
                {
                    if (parameters != null)
                    {
                        log_.Debug("Execute():  Getting Interaction parameter");
                        IInteraction interaction = (IInteraction)parameters.TryGetValue(INTERACTION);
                        if (interaction is IInteractionWorkItem || interaction is IInteraction)
                        {
                            log_.Debug("Execute():  The interaction is a WorkItem.");
                            var allAttachedData = interaction.GetAllAttachedData();
                            if (allAttachedData != null)
                            {
                                log_.Debug("Execute():  Checking Attached Data for IWD_mirroringTask");
                                if (allAttachedData.ContainsKey("IWD_mirroringTask"))
                                {
                                    String iwdMirroringTask = interaction.GetAttachedData("IWD_mirroringTask").ToString();
                                    log_.Debug("Execute():  IWD_mirroringTask = " + iwdMirroringTask);
                                    if (!String.IsNullOrEmpty(iwdMirroringTask) && (iwdMirroringTask.Equals("true", StringComparison.OrdinalIgnoreCase)))
                                    {
                                        log_.Debug("Execute():  Getting CommandParameter");
                                        if (parameters.ContainsKey(COMMAND_PARAMETER))
                                        {
                                            IToasterMessageWindow toasterWindow = (IToasterMessageWindow)parameters.TryGetValue(COMMAND_PARAMETER);
                                            BroadcastMessageSystem bms = (BroadcastMessageSystem)toasterWindow.Model.Message;
                                            String subject = bms.Subject;
                                            log_.Debug("Execute():  Getting the Subject of the toast: " + subject);
                                            if (subject.Contains(UNWANTED_SUBJECT))
                                            {
                                                log_.Debug("Execute():  Blocking the toast.");
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log_.Error("Execute(): Exception: " + e.Message);
            }
            log_.Debug("Execute(): Returning false.");
            log_.Debug("Execute(): END");
            return false;
        }

        /// <summary>
        /// This delegate allows access to the main thread.
        /// </summary>
        delegate bool ExecuteDelegate(IDictionary<string, object> parameters, IProgressUpdater progressUpdater);
    }
}
