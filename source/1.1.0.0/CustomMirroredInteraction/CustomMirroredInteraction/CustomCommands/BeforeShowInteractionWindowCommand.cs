﻿using Genesyslab.Desktop.Infrastructure.Commands;
using Genesyslab.Desktop.Infrastructure.DependencyInjection;
using Genesyslab.Desktop.Modules.Core.Model.Interactions;
using Genesyslab.Desktop.Modules.OpenMedia.Model.Interactions.WorkItem;
using Genesyslab.Desktop.Modules.Windows.Interactions;
using Genesyslab.Platform.Commons.Logging;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;

namespace Genesyslab.Desktop.Modules.CustomMirroredInteraction.CustomCommands
{
    /// <summary>
    /// The following custom WDE implementation will automatically minimize the Interactions Window of a Mirrored Interaction.
    ///   1.	Insert a new custom command: BeforeShowInteractionsWindowCommand
    ///       a.	The command is always active unless Role Privilege “Custom – Can Use Mirrored Test Mode” is allowed.
    ///       b.	Get the IInteraction from the Interaction parameter
    ///           i.	If the IInteraction is IInteractionWorkItem
    ///               1.	If KVP “IWD_mirroringTask” = “TRUE” (not case-sensitive)
    ///                   a.	Get the IInteractionsWindow from the CommandParameter
    ///                       i.	Hide the IInteractionsWindow
    /// </summary>
    class BeforeShowInteractionsWindowCommand : IElementOfCommand
    {
        private const String COMMAND_PARAMETER = "CommandParameter";
        private const String INTERACTION_PARAMETER = "Interaction";
        private ILogger log_ = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="BeforeShowInteractionsWindowCommand"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public BeforeShowInteractionsWindowCommand(IObjectContainer container)
        {
            try
            {
                log_ = container.Resolve<ILogger>();

                // Create a child trace section.
                log_ = log_.CreateChildLogger("Genesyslab.Desktop.Modules.CustomMirroredInteraction.CustomCommands.BeforeShowInteractionsWindowCommand");
                log_.Debug("Constructor():  START");
            }
            catch (Exception e)
            {
                log_.Error("Constructor():  Exception: " + e.Message);
            }
            log_.Debug("Constructor():  END");
        }

        /// <summary>
        /// Gets the name of the command. This is optional.
        /// </summary>
        /// <value>The command name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>True if the execution is stopped; false if successful.</returns>
        public bool Execute(IDictionary<string, object> parameters, IProgressUpdater progress)
        {
            try
            {
                log_.Debug("Execute():  START");

                // To the main thread.
                if (Application.Current.Dispatcher != null && !Application.Current.Dispatcher.CheckAccess())
                {
                    object result = Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new ExecuteDelegate(Execute), parameters, progress);
                    return (bool)result;
                }
                else
                {
                    if (parameters != null)
                    {
                        log_.Debug("Execute():  Getting Interaction parameter");
                        if (parameters.ContainsKey(INTERACTION_PARAMETER))
                        {
                            object ixn = null;
                            if (parameters.TryGetValue(INTERACTION_PARAMETER, out ixn) && ixn is IInteraction)
                            {
                                IInteraction interaction = ixn as IInteraction;
                                if (interaction is IInteractionWorkItem || interaction is IInteraction)
                                {
                                    log_.Debug("Execute():  The interaction is a WorkItem.");
                                    var allAttachedData = interaction.GetAllAttachedData();
                                    if (allAttachedData != null)
                                    {
                                        log_.Debug("Execute():  Checking Attached Data for IWD_mirroringTask");
                                        if (allAttachedData.ContainsKey("IWD_mirroringTask"))
                                        {
                                            String iwdMirroringTask = interaction.GetAttachedData("IWD_mirroringTask").ToString();
                                            log_.Debug("Execute():  IWD_mirroringTask = " + iwdMirroringTask);
                                            if (!String.IsNullOrEmpty(iwdMirroringTask) && (iwdMirroringTask.Equals("true", StringComparison.OrdinalIgnoreCase)))
                                            {
                                                log_.Debug("Execute():  Getting CommandParameter");
                                                if (parameters.ContainsKey(COMMAND_PARAMETER))
                                                {
                                                    object windowObj = null;
                                                    if ((parameters.TryGetValue(COMMAND_PARAMETER, out windowObj)) && (windowObj is InteractionsWindow))
                                                    {
                                                        InteractionsWindow interactionsWindow = windowObj as InteractionsWindow;
                                                        log_.Debug("Execute():  Hiding the Interactions Window.");
                                                        interactionsWindow.Visibility = System.Windows.Visibility.Hidden;
                                                        log_.Debug("Execute():  Blocking Resizing.");
                                                        interactionsWindow.ResizeMode = ResizeMode.NoResize;
                                                        log_.Debug("Execute():  Blocking ShowInTaskbar.");
                                                        interactionsWindow.ShowInTaskbar = false;
                                                        log_.Debug("Execute():  Minimizing the Interactions Window.");
                                                        interactionsWindow.WindowState = WindowState.Minimized;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log_.Error("Execute(): Exception: " + e.Message);
            }
            log_.Debug("Execute(): Returning false.");
            log_.Debug("Execute(): END");
            return false;
        }

        /// <summary>
        /// This delegate allows access to the main thread.
        /// </summary>
        delegate bool ExecuteDelegate(IDictionary<string, object> parameters, IProgressUpdater progressUpdater);
    }
}
